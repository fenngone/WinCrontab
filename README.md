# WinCrontab

## About WinCrontab

WinCrontab is a simple implementation of Linux Crontab, but in Windows.  
The goal is to get this amazing tool on Windows.

## Contributing

Feel free to contribute. This programm is under MIT licence.
As mentionned in it, you just have to publish it in your futur code.

## Build

You can build this application for Windows, using simple c# compilator.  
I have personnaly used Microsoft Visual Studio IDE to build it.  


## Usage

This code is design to follow the exact cron syntax.  
So you can use it as usual :  


┌───────────── minute (0 - 59)  
│ ┌───────────── hour (0 - 23)  
│ │ ┌───────────── day of the month (1 - 31)  
│ │ │ ┌───────────── month (1 - 12)  
│ │ │ │ ┌───────────── day of the week (0 - 6) (Sunday to Saturday;  
│ │ │ │ │                                   7 is also Sunday on some systems)  
│ │ │ │ │  
│ │ │ │ │  
\* \* \* \* \* <command to execute>  

To get more information ,you can go [here](https://en.wikipedia.org/wiki/Cron)


