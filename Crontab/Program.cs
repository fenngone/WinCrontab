﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Management.Automation;
using System.Collections.ObjectModel;

namespace WinCrontab
{
    class Program

    {
        private static string folderPath = @"C:\Program Files\WinCrontab";
        private static string confPath = folderPath + @"\config.WinCrontab";

        static string getFolderPath()
        {
            return folderPath;
        }

        static string getConfPath()
        {
            return confPath;
        }

        static void excuteCommand(String command)
        {
            Thread thread = new Thread(() =>
            {
                try
                {
                    using (PowerShell powerShell = PowerShell.Create())
                    {
                        // Source functions.
                        powerShell.AddScript("Import-Module AppVPkgConverter");
                        powerShell.AddScript("Get-Command -Module AppVPkgConverter");
                        powerShell.AddScript(command);

                        // invoke execution on the pipeline (collecting output)
                        Collection<PSObject> PSOutput = powerShell.Invoke();

                        // loop through each output object item
                        foreach (PSObject outputItem in PSOutput)
                        {
                            // if null object was dumped to the pipeline during the script then a null object may be present here
                            if (outputItem != null)
                            {
                                Console.WriteLine($"{outputItem}");
                            }
                        }

                        // check the other output streams (for example, the error stream) and display them with advices.
                        if (powerShell.Streams.Error.Count > 0)
                        {
                            Console.WriteLine("Error when executing '" + command + "' , an error apear when trying to execute it whith PowerShell");
                            foreach(ErrorRecord error in powerShell.Streams.Error)
                            {
                                Console.WriteLine(error.ErrorDetails.Message + "\n--->" + error.ErrorDetails.RecommendedAction);
                            }
                        }
                    }

                }
                catch(Exception e)
                {
                    Console.WriteLine("Error when executing '" + command + "' , an error apear when trying to execute it whith PowerShell");
                }
            });
            thread.Start();
            return;
        }

        static Dictionary<string, int> getLstAutorizedValue(int i)
        {
            if (i == 3)
            {
                return new Dictionary<string, int>() { { "jan", 1 }, { "feb", 2 }, { "mar", 3 }, { "apr", 4 }, { "mai", 5 }, { "jun", 6 }, { "jul", 7 }, { "aug", 8 }, { "sep", 9 }, { "oct", 10 }, { "nov", 11 }, { "dec", 12 } };
            }
            else if (i == 4)
            {
                return new Dictionary<string, int>() { { "sun", 7 }, { "mon", 1 }, { "tue", 2 }, { "wed", 3 }, { "thu", 4 }, { "fri", 5 }, { "sat", 6 } };
            }
            else
            {
                return new Dictionary<string, int>(); //This will return false, without sending developpement error.
            }
        }

        static Boolean isOk(String args, int i)
        {
            var time = DateTime.Now;
            int max = 0;
            int min = 0;
            int value = 0;

            //Init constant depend to args number
            if (i == 0) { min = 0; max = 59; value = time.Minute; }
            else if (i == 1) { min = 0; max = 23; value = time.Hour; }
            else if (i == 2) { min = 1; max = 31; value = time.Hour; }
            else if (i == 3) { min = 1; max = 12; value = time.Month; }
            else if (i == 4)
            {
                min = 1; max = 7;
                value = (int)time.DayOfWeek;        //(int)time.DayOfWeek return a value between 0 and 6 with 0 == sunday, 1 monday , ...
                value = (value == 0) ? 7 : value;   //Converstion to set sunday as number 7
            }


            //Interval implementation


            if (args.Contains(","))
            {

                String[] lstRecursiveArgs = args.Split(',');

                //The boolean sum of every args should produce true to execute
                //In a crontab, we can find this case : 
                //<subArg1>,<subArg2>,<subArgs3>,... --->

                Boolean result = false;
                foreach (String recursiveArgs in lstRecursiveArgs)
                {
                    result |= isOk(recursiveArgs, i);
                }

                return result;
            }
            else if (args.Contains("-"))
            {
                String[] range = args.Split('-');
                if (range.Length != 2)
                {
                    Console.WriteLine("Syntax Error, only one '-' is allowed. Please do WinCrontab --help to discover right format");
                    return false;
                }

                try
                {
                    int begin = Int32.Parse(range[0]);
                    int end = Int32.Parse(range[1]);

                    if (begin < min || end > max) return false; //If the given interval is not possible

                    return ((begin <= value) && (value <= end));
                }
                catch (Exception e)
                {
                    //It's not number, so check if it's common day,week, ...

                    Dictionary<String, int> lstAutorizedValue = getLstAutorizedValue(i);

                    if (i == 3 || i == 4)
                    {
                        if (lstAutorizedValue.ContainsKey(range[0].ToLower()) && lstAutorizedValue.ContainsKey(range[1].ToLower()))
                        {

                            if (lstAutorizedValue[range[0].ToLower()] <= value && value <= lstAutorizedValue[range[1].ToLower()])
                            {
                                return true; //The value given is in the right interval
                            }
                            else if (lstAutorizedValue[range[0].ToLower()] <= lstAutorizedValue[range[1].ToLower()])
                            {
                                return false;
                            }
                            else
                            {
                                Console.WriteLine("Syntax Error, " + range[0] + " must be lower than " + range[1]);
                                return false;
                            }
                        }
                        else
                        {
                            Console.WriteLine("Syntax Error, all value are not in the given liste, please do WinCrontab --help to discover right format");
                            return false;
                        }
                    }
                    return false;
                }
            }
            else if (args.Contains("/"))
            {

                if (args.Split('/').Length != 2)
                {
                    Console.WriteLine("Syntax Error, only one / is allowed. Please do WinCrontab --help to discover right format");
                    return false;
                }

                try
                {

                    if (i >= 0 && i < 3)
                    {
                        int interval = Int32.Parse(args.Split('/')[1]);
                        int begin = (args.Split('/')[0].Trim().Equals("*") || args.Split('/')[0].Trim().Equals("")) ? 0 : Int32.Parse(args.Split('/')[0]);

                        return ((value >= begin) && ((value % interval) == 0));
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("2) Syntax error '/' can only be use for minute hour or day NUMBER, string are not allowed here");
                    return false;
                }
            }
            else
            {
                if (args.Trim().Equals("*")) return true;

                try
                {
                    return (Int32.Parse(args) == value);
                }
                catch (Exception e)
                {
                    //It's not number, so check if it's common day,week, ...
                    Dictionary<String, int> lstAutorizedValue = getLstAutorizedValue(i);

                    if (lstAutorizedValue.ContainsKey(args.ToLower()))
                    {
                        return (lstAutorizedValue[args.ToLower()] == value);
                    }
                    else
                    {
                        Console.WriteLine("Syntax Error, all value are not in the given liste, please do WinCrontab --help to discover right format");
                        return false;
                    }

                }
            }
            return false;
        }

        static void initFile()
        {

            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }


            if (!File.Exists(confPath))
            {
                File.Create(confPath).Close();
                TextWriter tw = new StreamWriter(confPath);
                tw.WriteLine("#WinCrontab Configuration file");
                tw.WriteLine("#This is a comment line, which will not be executed\n\n");
                tw.WriteLine("# Example of job definition:");
                tw.WriteLine("# .---------------- minute (0 - 59)");
                tw.WriteLine("# |  .------------- hour (0 - 23)");
                tw.WriteLine("# |  |  .---------- day of month (1 - 31)");
                tw.WriteLine("# |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...");
                tw.WriteLine("# |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat");
                tw.WriteLine("# |  |  |  |  |");
                tw.WriteLine("# *  *  *  *  *  user command to be executed");
                tw.Close();
            }
            else if (File.Exists(confPath))
            {
                TextReader tr = new StreamReader(confPath);
                Console.WriteLine("Get current config");
            }
        }


        static void Main(string[] args)
        {
            if (args.Length == 1)
            {
                if (args[0][0] != '-')
                {
                    Console.WriteLine("Please use : WinCrontab --help to get help about programms");
                    return;
                }

                if (args[0] == "--help")
                {
                    Console.WriteLine("This is WinCrontab Man\n");
                    Console.WriteLine("-l : display current config");
                    Console.WriteLine("-r : remove each crontab action");
                    Console.WriteLine("-e : edit contab actions");
                    return;
                }


                //Let's check if one arg is call many time.
                String remember = "";
                for (int i = 0; i < args[0].Length; i++)
                {
                    if (!remember.Contains(args[0][i].ToString()))
                    {
                        remember += args[0][i];
                    }
                    else
                    {
                        Console.WriteLine("You can't repeat and args...");
                        return;
                    }
                }

                initFile();
                for (int i = 0; i < args[0].Length; i++)
                {
                    switch (args[0][i])
                    {
                        case 'l':

                            string[] lines = File.ReadAllLines(getConfPath());
                            foreach (string line in lines) Console.WriteLine(line);
                            break;

                        case 'r':

                            File.Delete(getConfPath());
                            File.Create(getConfPath()).Close();

                            break;

                        case 'e':
                            var OpenFile = new System.IO.StreamReader(getConfPath());



                            OpenFile.Close();
                            break;
                    }

                }
            }
            else if (args.Length == 0)
            {
                initFile();
                long nextCheckTimeStamp = ((DateTimeOffset)DateTime.Now).ToUnixTimeSeconds()-(((DateTimeOffset)DateTime.Now).ToUnixTimeSeconds()%60);
                nextCheckTimeStamp += 60;     //

                Console.WriteLine("Process ... Please don't stop me, otherwise, no cron should be launch");
                while (true)
                {

                    //Skip if one sec is not pass
                    
                    if (nextCheckTimeStamp > ((DateTimeOffset)DateTime.Now).ToUnixTimeSeconds()) continue;
                    Console.WriteLine(nextCheckTimeStamp + " < " + ((DateTimeOffset)DateTime.Now).ToUnixTimeSeconds());

                    string[] lines = File.ReadAllLines(getConfPath());
                    int numLine = 0;
                    foreach (string line in lines)
                    {

                        if (line =="") continue;
                        if (line[0].Equals('#')) continue;
                        if (line.Split(' ').Length < 2)
                        {
                            Console.WriteLine("Error next to line " + numLine + " not enough args to create a cron");
                            continue;
                        }

                        /*
                          
                                @yearly (or @annually)	Run once a year at midnight of 1 January	0 0 1 1 *
                                @monthly	Run once a month at midnight of the first day of the month	0 0 1 * *
                                @weekly	Run once a week at midnight on Sunday morning	0 0 * * 0
                                @daily (or @midnight)	Run once a day at midnight	0 0 * * *
                                @hourly	Run once an hour at the beginning of the hour	0 * * * *
                                @reboot -----> Not implemented

                         */

                        if (line.Split(' ')[0][0].Equals('@'))
                        {
                            DateTime time = DateTime.Now;
                            String cmd = "";
                            for (int i = 1; i < line.Split(' ').Length; i++) cmd += (line.Split(' ').Length == i + 1)? line.Split(' ')[i] : line.Split(' ')[i] + " ";

                            switch (line.Split(' ')[0].ToLower())
                            {
                                case "@yearly":
                                case "@annually":
                                    if (time.DayOfYear == 1 && time.Hour == 0 && time.Minute == 0) excuteCommand(cmd);
                                    break;

                                case "@monthly":
                                    if (time.DayOfWeek == 0 && time.Hour == 0 && time.Minute == 0) excuteCommand(cmd);
                                    break;

                                case "@weekly":
                                    if (time.DayOfWeek == 0 && time.Hour == 0 && time.Minute == 0) excuteCommand(cmd);
                                    break;

                                case "@daily":
                                case "@midnight":
                                    if (time.Hour == 0 && time.Minute == 0) excuteCommand(cmd);
                                    break;

                                case "@hourly":
                                    if (time.Minute == 0) excuteCommand(cmd);
                                    break;

                            }
                        }

                        if (line.Split(' ').Length < 6)
                        {
                            Console.WriteLine("Error next to line " + numLine + " not enough args to create a cron");
                            continue;
                        }

                        args = line.Split(' ');

                        String command = "";
                        Boolean canLaunch = true;
                        int cpt = 0;
                        for (int i = 0; i < args.Length; i++)
                        {
                            if (args[i] == "") continue;
                            if (cpt <= 4) canLaunch &= isOk(args[i], cpt);
                            else command += (i == args.Length - 1) ? args[i] : args[i] + " ";
                            cpt++;
                        }
                        numLine++;

                        if (canLaunch)
                        {
                            excuteCommand(command);
                        }
                    }
                    nextCheckTimeStamp += 60;
                }
            }
        }
    }


}




